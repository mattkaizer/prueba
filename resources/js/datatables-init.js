// Databases inicializador
$(document).ready(function() {
    // Table ID
    var table = $('#dataTable');
    
    table.DataTable({
      responsive: true,
      dom: 'Bfrtip',
      buttons: [
        { extend: 'colvis', className: 'btn btn-primary' },
        { extend: 'excel', className: 'btn btn-primary' },
        { extend: 'print', className: 'btn btn-primary' },
        { extend: 'pdf', className: 'btn btn-primary' },
        //{ extend: 'csv', className: 'btn btn-primary' },
        { extend: 'pageLength', className: 'btn btn-primary' },
      ],
      "language": {
        "buttons": {
          "print" : 'Imprimir',
          "colvis": 'Columnas',
          "pageLength": {
              "_": "Mostrar %d",
              "-1": "",
          }
        },
        "bPrint":          "Imprimir",
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      }
      
    } );
  
    //Insertar input para busqueda en el footer
    $('#dataTable tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Buscar '+ title +'" />' );
    } );
  
    //Busqueda
    table.DataTable().columns().every( function () {
        var that = this;
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
    
    $('#dataTable_filter input').attr("placeholder", "Buscar");
  }); //End document ready
  
  
  