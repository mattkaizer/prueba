<!-- Divider -->
<hr class="sidebar-divider">
<li class="{{ Request::is('people*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('people.index') !!}"><i class="fas fa-fw fa-table"></i><span>People</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider">
<li class="{{ Request::is('contactInfos*') ? 'active' : '' }} nav-item">
    <a class="nav-link" href="{!! route('contactInfos.index') !!}"><i class="fas fa-fw fa-table"></i><span>Contact Infos</span></a>
</li>

