<!-- N Expediente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('n_expediente', 'N Expediente:') !!}
    {!! Form::text('n_expediente', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Alta Acufade Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_alta_acufade', 'Fecha Alta Acufade:') !!}
    {!! Form::date('fecha_alta_acufade', null, ['class' => 'form-control','id'=>'fecha_alta_acufade']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#fecha_alta_acufade').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Foto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('foto', 'Foto:') !!}
    {!! Form::file('foto') !!}
</div>
<div class="clearfix"></div>

<!-- Nombre Completo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_completo', 'Nombre Completo:') !!}
    {!! Form::text('nombre_completo', null, ['class' => 'form-control']) !!}
</div>

<!-- Alias Field -->
<div class="form-group col-sm-6">
    {!! Form::label('alias', 'Alias:') !!}
    {!! Form::text('alias', null, ['class' => 'form-control']) !!}
</div>

<!-- Genero Field -->
<div class="form-group col-sm-6">
    {!! Form::label('genero', 'Genero:') !!}
    {!! Form::select('genero', [' Hombre' => ' Hombre', ' Mujer' => ' Mujer'], null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Naci Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_naci', 'Fecha Naci:') !!}
    {!! Form::date('fecha_naci', null, ['class' => 'form-control','id'=>'fecha_naci']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#fecha_naci').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Edad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('edad', 'Edad:') !!}
    {!! Form::text('edad', null, ['class' => 'form-control']) !!}
</div>

<!-- Dni Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dni', 'Dni:') !!}
    {!! Form::text('dni', null, ['class' => 'form-control']) !!}
</div>

<!-- Est Civil Field -->
<div class="form-group col-sm-6">
    {!! Form::label('est_civil', 'Est Civil:') !!}
    {!! Form::text('est_civil', null, ['class' => 'form-control']) !!}
</div>

<!-- Grado Dependencia Field -->
<div class="form-group col-sm-6">
    {!! Form::label('grado_dependencia', 'Grado Dependencia:') !!}
    {!! Form::select('grado_dependencia', [' No valorado' => ' No valorado', ' Grado I' => ' Grado I', ' Grado II' => ' Grado II', ' Grado III' => ' Grado III'], null, ['class' => 'form-control']) !!}
</div>

<!-- Transporte Field -->
<div class="form-group col-sm-6">
    {!! Form::label('transporte', 'Transporte:') !!}
    {!! Form::select('transporte', [' Si' => ' Si', ' No' => ' No'], null, ['class' => 'form-control']) !!}
</div>

<!-- Transp Dias Field -->
<div class="form-group col-sm-6">
    {!! Form::label('transp_dias', 'Transp Dias:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('transp_dias', 0) !!}
        {!! Form::checkbox('transp_dias', ' Lunes', null) !!}  Lunes
    </label>
</div>

<!-- Trayecto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('trayecto', 'Trayecto:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('trayecto', 0) !!}
        {!! Form::checkbox('trayecto', ' Ida', null) !!}  Ida
    </label>
</div>

<!-- Transp Fecha Init Field -->
<div class="form-group col-sm-6">
    {!! Form::label('transp_fecha_init', 'Transp Fecha Init:') !!}
    {!! Form::date('transp_fecha_init', null, ['class' => 'form-control','id'=>'transp_fecha_init']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#transp_fecha_init').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Transp Dir Recogida Field -->
<div class="form-group col-sm-6">
    {!! Form::label('transp_dir_recogida', 'Transp Dir Recogida:') !!}
    {!! Form::text('transp_dir_recogida', null, ['class' => 'form-control']) !!}
</div>

<!-- Cesion Imagen Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cesion_imagen', 'Cesion Imagen:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('cesion_imagen', 0) !!}
        {!! Form::checkbox('cesion_imagen', ' Si', null) !!}  Si
    </label>
</div>

<!-- Autoriz Salida Ext Field -->
<div class="form-group col-sm-6">
    {!! Form::label('autoriz_salida_ext', 'Autoriz Salida Ext:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('autoriz_salida_ext', 0) !!}
        {!! Form::checkbox('autoriz_salida_ext', ' Si', null) !!}  Si
    </label>
</div>

<!-- Autoriz Salir Centro Field -->
<div class="form-group col-sm-6">
    {!! Form::label('autoriz_salir_centro', 'Autoriz Salir Centro:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('autoriz_salir_centro', 0) !!}
        {!! Form::checkbox('autoriz_salir_centro', ' Si', null) !!}  Si
    </label>
</div>

<!-- Socio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('socio', 'Socio:') !!}
    {!! Form::select('socio', [' Si' => ' Si', ' No' => ' No'], null, ['class' => 'form-control']) !!}
</div>

<!-- Num Socio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('num_socio', 'Num Socio:') !!}
    {!! Form::text('num_socio', null, ['class' => 'form-control']) !!}
</div>

<!-- Nivel Educativo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nivel_educativo', 'Nivel Educativo:') !!}
    {!! Form::select('nivel_educativo', [' Lee y escribe' => ' Lee y escribe', ' No lee ni escribe' => ' No lee ni escribe', ' Graduado Escolar' => ' Graduado Escolar', ' Bachillerato' => ' Bachillerato', ' FPGM' => ' FPGM', ' FPGS' => ' FPGS', ' Estudios Superiores' => ' Estudios Superiores'], null, ['class' => 'form-control']) !!}
</div>

<!-- Estudios Terminados Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estudios_terminados', 'Estudios Terminados:') !!}
    {!! Form::select('estudios_terminados', [' Si' => ' Si', ' No' => ' No'], null, ['class' => 'form-control']) !!}
</div>

<!-- Ocupacion Anterior Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ocupacion_anterior', 'Ocupacion Anterior:') !!}
    {!! Form::text('ocupacion_anterior', null, ['class' => 'form-control']) !!}
</div>

<!-- Inter Actuales Field -->
<div class="form-group col-sm-6">
    {!! Form::label('inter_actuales', 'Inter Actuales:') !!}
    {!! Form::text('inter_actuales', null, ['class' => 'form-control']) !!}
</div>

<!-- Lee Escribe Ahora Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lee_escribe_ahora', 'Lee Escribe Ahora:') !!}
    {!! Form::select('lee_escribe_ahora', [' Si' => ' Si', ' No' => ' No'], null, ['class' => 'form-control']) !!}
</div>

<!-- Sit Dependencia Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sit_dependencia', 'Sit Dependencia:') !!}
    {!! Form::select('sit_dependencia', [' No solicitud' => ' No solicitud', ' Solicitud' => ' Solicitud', ' Espera Grado' => ' Espera Grado', ' Espera PEVS' => ' Espera PEVS', ' Recibe PEVS' => ' Recibe PEVS'], null, ['class' => 'form-control']) !!}
</div>

<!-- Grado Dependencia Field -->
<div class="form-group col-sm-6">
    {!! Form::label('grado_dependencia', 'Grado Dependencia:') !!}
    {!! Form::select('grado_dependencia', [' I' => ' I', ' II' => ' II', ' III' => ' III', ' Espera Revisión' => ' Espera Revisión'], null, ['class' => 'form-control']) !!}
</div>

<!-- Nivel Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nivel', 'Nivel:') !!}
    {!! Form::select('nivel', [' I' => ' I', ' II' => ' II', ' III' => ' III'], null, ['class' => 'form-control']) !!}
</div>

<!-- Tipo Ayuda Depend Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo_ayuda_depend', 'Tipo Ayuda Depend:') !!}
    {!! Form::select('tipo_ayuda_depend', [' PEVS' => ' PEVS', ' Cuidados' => ' Cuidados', ' Entornos' => ' Entornos', ' Otras' => ' Otras'], null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Solic Grado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_solic_grado', 'Fecha Solic Grado:') !!}
    {!! Form::date('fecha_solic_grado', null, ['class' => 'form-control','id'=>'fecha_solic_grado']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#fecha_solic_grado').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Fecha Resol Grado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_resol_grado', 'Fecha Resol Grado:') !!}
    {!! Form::date('fecha_resol_grado', null, ['class' => 'form-control','id'=>'fecha_resol_grado']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#fecha_resol_grado').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Fecha Resol Prest Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_resol_prest', 'Fecha Resol Prest:') !!}
    {!! Form::date('fecha_resol_prest', null, ['class' => 'form-control','id'=>'fecha_resol_prest']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#fecha_resol_prest').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Fecha Revision Grado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_revision_grado', 'Fecha Revision Grado:') !!}
    {!! Form::date('fecha_revision_grado', null, ['class' => 'form-control','id'=>'fecha_revision_grado']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#fecha_revision_grado').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Cuantia Resol Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cuantia_resol', 'Cuantia Resol:') !!}
    {!! Form::text('cuantia_resol', null, ['class' => 'form-control']) !!}
</div>

<!-- Tecn Nom Ape Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tecn_nom_Ape', 'Tecn Nom Ape:') !!}
    {!! Form::text('tecn_nom_Ape', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Tecn Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email_tecn', 'Email Tecn:') !!}
    {!! Form::email('email_tecn', null, ['class' => 'form-control']) !!}
</div>

<!-- Tel Tecn Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tel_tecn', 'Tel Tecn:') !!}
    {!! Form::text('tel_tecn', null, ['class' => 'form-control']) !!}
</div>

<!-- Tipo Ayuda Sociosani Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo_ayuda_sociosani', 'Tipo Ayuda Sociosani:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('tipo_ayuda_sociosani', 0) !!}
        {!! Form::checkbox('tipo_ayuda_sociosani', ' Teleasistencia', null) !!}  Teleasistencia
    </label>
</div>

<!-- Certi Discapacidad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('certi_discapacidad', 'Certi Discapacidad:') !!}
    {!! Form::select('certi_discapacidad', [' Si' => ' Si', ' No' => ' No'], null, ['class' => 'form-control']) !!}
</div>

<!-- Grado Discapacidad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('grado_discapacidad', 'Grado Discapacidad:') !!}
    {!! Form::text('grado_discapacidad', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Resol Discapacidad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_resol_discapacidad', 'Fecha Resol Discapacidad:') !!}
    {!! Form::date('fecha_resol_discapacidad', null, ['class' => 'form-control','id'=>'fecha_resol_discapacidad']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#fecha_resol_discapacidad').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Tipo Discapacidad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo_discapacidad', 'Tipo Discapacidad:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('tipo_discapacidad', 0) !!}
        {!! Form::checkbox('tipo_discapacidad', ' Intelectual', null) !!}  Intelectual
    </label>
</div>

<!-- Sit Legal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sit_legal', 'Sit Legal:') !!}
    {!! Form::select('sit_legal', [' Autónomo/a' => ' Autónomo/a', ' Tutela' => ' Tutela', ' Curatela' => ' Curatela', ' Guarda de hecho' => ' Guarda de hecho'], null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('people.index') !!}" class="btn btn-default">Cancel</a>
</div>
