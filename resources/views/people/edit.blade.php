@extends('layouts.home')
@section('title', "Edit")
@section('content')

<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Person</h1>
    <br>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Edition</h6>
        </div>
        <div class="card-body">
            {!! Form::model($person, ['route' => ['people.update', $person->id], 'method' => 'patch']) !!}

            @include('people.fields')

            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection