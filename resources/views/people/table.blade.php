<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>N Expediente</th>
        <th>Fecha Alta Acufade</th>
        <th>Foto</th>
        <th>Nombre Completo</th>
        <th>Alias</th>
        <th>Genero</th>
        <th>Fecha Naci</th>
        <th>Edad</th>
        <th>Dni</th>
        <th>Est Civil</th>
        <th>Grado Dependencia</th>
        <th>Transporte</th>
        <th>Transp Dias</th>
        <th>Trayecto</th>
        <th>Transp Fecha Init</th>
        <th>Transp Dir Recogida</th>
        <th>Cesion Imagen</th>
        <th>Autoriz Salida Ext</th>
        <th>Autoriz Salir Centro</th>
        <th>Socio</th>
        <th>Num Socio</th>
        <th>Nivel Educativo</th>
        <th>Estudios Terminados</th>
        <th>Ocupacion Anterior</th>
        <th>Inter Actuales</th>
        <th>Lee Escribe Ahora</th>
        <th>Sit Dependencia</th>
        <th>Grado Dependencia</th>
        <th>Nivel</th>
        <th>Tipo Ayuda Depend</th>
        <th>Fecha Solic Grado</th>
        <th>Fecha Resol Grado</th>
        <th>Fecha Resol Prest</th>
        <th>Fecha Revision Grado</th>
        <th>Cuantia Resol</th>
        <th>Tecn Nom Ape</th>
        <th>Email Tecn</th>
        <th>Tel Tecn</th>
        <th>Tipo Ayuda Sociosani</th>
        <th>Certi Discapacidad</th>
        <th>Grado Discapacidad</th>
        <th>Fecha Resol Discapacidad</th>
        <th>Tipo Discapacidad</th>
        <th>Sit Legal</th>
            <th>Action</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>N Expediente</th>
        <th>Fecha Alta Acufade</th>
        <th>Foto</th>
        <th>Nombre Completo</th>
        <th>Alias</th>
        <th>Genero</th>
        <th>Fecha Naci</th>
        <th>Edad</th>
        <th>Dni</th>
        <th>Est Civil</th>
        <th>Grado Dependencia</th>
        <th>Transporte</th>
        <th>Transp Dias</th>
        <th>Trayecto</th>
        <th>Transp Fecha Init</th>
        <th>Transp Dir Recogida</th>
        <th>Cesion Imagen</th>
        <th>Autoriz Salida Ext</th>
        <th>Autoriz Salir Centro</th>
        <th>Socio</th>
        <th>Num Socio</th>
        <th>Nivel Educativo</th>
        <th>Estudios Terminados</th>
        <th>Ocupacion Anterior</th>
        <th>Inter Actuales</th>
        <th>Lee Escribe Ahora</th>
        <th>Sit Dependencia</th>
        <th>Grado Dependencia</th>
        <th>Nivel</th>
        <th>Tipo Ayuda Depend</th>
        <th>Fecha Solic Grado</th>
        <th>Fecha Resol Grado</th>
        <th>Fecha Resol Prest</th>
        <th>Fecha Revision Grado</th>
        <th>Cuantia Resol</th>
        <th>Tecn Nom Ape</th>
        <th>Email Tecn</th>
        <th>Tel Tecn</th>
        <th>Tipo Ayuda Sociosani</th>
        <th>Certi Discapacidad</th>
        <th>Grado Discapacidad</th>
        <th>Fecha Resol Discapacidad</th>
        <th>Tipo Discapacidad</th>
        <th>Sit Legal</th>
            <th>Action</th>
        </tr>
    </tfoot>
    <tbody>
    @foreach($people as $person)
        <tr>
            <td>{!! $person->n_expediente !!}</td>
            <td>{!! $person->fecha_alta_acufade !!}</td>
            <td>{!! $person->foto !!}</td>
            <td>{!! $person->nombre_completo !!}</td>
            <td>{!! $person->alias !!}</td>
            <td>{!! $person->genero !!}</td>
            <td>{!! $person->fecha_naci !!}</td>
            <td>{!! $person->edad !!}</td>
            <td>{!! $person->dni !!}</td>
            <td>{!! $person->est_civil !!}</td>
            <td>{!! $person->grado_dependencia !!}</td>
            <td>{!! $person->transporte !!}</td>
            <td>{!! $person->transp_dias !!}</td>
            <td>{!! $person->trayecto !!}</td>
            <td>{!! $person->transp_fecha_init !!}</td>
            <td>{!! $person->transp_dir_recogida !!}</td>
            <td>{!! $person->cesion_imagen !!}</td>
            <td>{!! $person->autoriz_salida_ext !!}</td>
            <td>{!! $person->autoriz_salir_centro !!}</td>
            <td>{!! $person->socio !!}</td>
            <td>{!! $person->num_socio !!}</td>
            <td>{!! $person->nivel_educativo !!}</td>
            <td>{!! $person->estudios_terminados !!}</td>
            <td>{!! $person->ocupacion_anterior !!}</td>
            <td>{!! $person->inter_actuales !!}</td>
            <td>{!! $person->lee_escribe_ahora !!}</td>
            <td>{!! $person->sit_dependencia !!}</td>
            <td>{!! $person->grado_dependencia !!}</td>
            <td>{!! $person->nivel !!}</td>
            <td>{!! $person->tipo_ayuda_depend !!}</td>
            <td>{!! $person->fecha_solic_grado !!}</td>
            <td>{!! $person->fecha_resol_grado !!}</td>
            <td>{!! $person->fecha_resol_prest !!}</td>
            <td>{!! $person->fecha_revision_grado !!}</td>
            <td>{!! $person->cuantia_resol !!}</td>
            <td>{!! $person->tecn_nom_Ape !!}</td>
            <td>{!! $person->email_tecn !!}</td>
            <td>{!! $person->tel_tecn !!}</td>
            <td>{!! $person->tipo_ayuda_sociosani !!}</td>
            <td>{!! $person->certi_discapacidad !!}</td>
            <td>{!! $person->grado_discapacidad !!}</td>
            <td>{!! $person->fecha_resol_discapacidad !!}</td>
            <td>{!! $person->tipo_discapacidad !!}</td>
            <td>{!! $person->sit_legal !!}</td>
            <td>
                {!! Form::open(['route' => ['people.destroy', $person->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('people.show', [$person->id]) !!}" class='btn btn-primary btn-xs'><i class="far fa-eye"></i></a>
                    <a href="{!! route('people.edit', [$person->id]) !!}" class='btn btn-primary btn-xs'><i class="far fa-edit"></i></a>
                    {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>