<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $person->id !!}</p>
</div>

<!-- N Expediente Field -->
<div class="form-group">
    {!! Form::label('n_expediente', 'N Expediente:') !!}
    <p>{!! $person->n_expediente !!}</p>
</div>

<!-- Fecha Alta Acufade Field -->
<div class="form-group">
    {!! Form::label('fecha_alta_acufade', 'Fecha Alta Acufade:') !!}
    <p>{!! $person->fecha_alta_acufade !!}</p>
</div>

<!-- Foto Field -->
<div class="form-group">
    {!! Form::label('foto', 'Foto:') !!}
    <p>{!! $person->foto !!}</p>
</div>

<!-- Nombre Completo Field -->
<div class="form-group">
    {!! Form::label('nombre_completo', 'Nombre Completo:') !!}
    <p>{!! $person->nombre_completo !!}</p>
</div>

<!-- Alias Field -->
<div class="form-group">
    {!! Form::label('alias', 'Alias:') !!}
    <p>{!! $person->alias !!}</p>
</div>

<!-- Genero Field -->
<div class="form-group">
    {!! Form::label('genero', 'Genero:') !!}
    <p>{!! $person->genero !!}</p>
</div>

<!-- Fecha Naci Field -->
<div class="form-group">
    {!! Form::label('fecha_naci', 'Fecha Naci:') !!}
    <p>{!! $person->fecha_naci !!}</p>
</div>

<!-- Edad Field -->
<div class="form-group">
    {!! Form::label('edad', 'Edad:') !!}
    <p>{!! $person->edad !!}</p>
</div>

<!-- Dni Field -->
<div class="form-group">
    {!! Form::label('dni', 'Dni:') !!}
    <p>{!! $person->dni !!}</p>
</div>

<!-- Est Civil Field -->
<div class="form-group">
    {!! Form::label('est_civil', 'Est Civil:') !!}
    <p>{!! $person->est_civil !!}</p>
</div>

<!-- Grado Dependencia Field -->
<div class="form-group">
    {!! Form::label('grado_dependencia', 'Grado Dependencia:') !!}
    <p>{!! $person->grado_dependencia !!}</p>
</div>

<!-- Transporte Field -->
<div class="form-group">
    {!! Form::label('transporte', 'Transporte:') !!}
    <p>{!! $person->transporte !!}</p>
</div>

<!-- Transp Dias Field -->
<div class="form-group">
    {!! Form::label('transp_dias', 'Transp Dias:') !!}
    <p>{!! $person->transp_dias !!}</p>
</div>

<!-- Trayecto Field -->
<div class="form-group">
    {!! Form::label('trayecto', 'Trayecto:') !!}
    <p>{!! $person->trayecto !!}</p>
</div>

<!-- Transp Fecha Init Field -->
<div class="form-group">
    {!! Form::label('transp_fecha_init', 'Transp Fecha Init:') !!}
    <p>{!! $person->transp_fecha_init !!}</p>
</div>

<!-- Transp Dir Recogida Field -->
<div class="form-group">
    {!! Form::label('transp_dir_recogida', 'Transp Dir Recogida:') !!}
    <p>{!! $person->transp_dir_recogida !!}</p>
</div>

<!-- Cesion Imagen Field -->
<div class="form-group">
    {!! Form::label('cesion_imagen', 'Cesion Imagen:') !!}
    <p>{!! $person->cesion_imagen !!}</p>
</div>

<!-- Autoriz Salida Ext Field -->
<div class="form-group">
    {!! Form::label('autoriz_salida_ext', 'Autoriz Salida Ext:') !!}
    <p>{!! $person->autoriz_salida_ext !!}</p>
</div>

<!-- Autoriz Salir Centro Field -->
<div class="form-group">
    {!! Form::label('autoriz_salir_centro', 'Autoriz Salir Centro:') !!}
    <p>{!! $person->autoriz_salir_centro !!}</p>
</div>

<!-- Socio Field -->
<div class="form-group">
    {!! Form::label('socio', 'Socio:') !!}
    <p>{!! $person->socio !!}</p>
</div>

<!-- Num Socio Field -->
<div class="form-group">
    {!! Form::label('num_socio', 'Num Socio:') !!}
    <p>{!! $person->num_socio !!}</p>
</div>

<!-- Nivel Educativo Field -->
<div class="form-group">
    {!! Form::label('nivel_educativo', 'Nivel Educativo:') !!}
    <p>{!! $person->nivel_educativo !!}</p>
</div>

<!-- Estudios Terminados Field -->
<div class="form-group">
    {!! Form::label('estudios_terminados', 'Estudios Terminados:') !!}
    <p>{!! $person->estudios_terminados !!}</p>
</div>

<!-- Ocupacion Anterior Field -->
<div class="form-group">
    {!! Form::label('ocupacion_anterior', 'Ocupacion Anterior:') !!}
    <p>{!! $person->ocupacion_anterior !!}</p>
</div>

<!-- Inter Actuales Field -->
<div class="form-group">
    {!! Form::label('inter_actuales', 'Inter Actuales:') !!}
    <p>{!! $person->inter_actuales !!}</p>
</div>

<!-- Lee Escribe Ahora Field -->
<div class="form-group">
    {!! Form::label('lee_escribe_ahora', 'Lee Escribe Ahora:') !!}
    <p>{!! $person->lee_escribe_ahora !!}</p>
</div>

<!-- Sit Dependencia Field -->
<div class="form-group">
    {!! Form::label('sit_dependencia', 'Sit Dependencia:') !!}
    <p>{!! $person->sit_dependencia !!}</p>
</div>

<!-- Grado Dependencia Field -->
<div class="form-group">
    {!! Form::label('grado_dependencia', 'Grado Dependencia:') !!}
    <p>{!! $person->grado_dependencia !!}</p>
</div>

<!-- Nivel Field -->
<div class="form-group">
    {!! Form::label('nivel', 'Nivel:') !!}
    <p>{!! $person->nivel !!}</p>
</div>

<!-- Tipo Ayuda Depend Field -->
<div class="form-group">
    {!! Form::label('tipo_ayuda_depend', 'Tipo Ayuda Depend:') !!}
    <p>{!! $person->tipo_ayuda_depend !!}</p>
</div>

<!-- Fecha Solic Grado Field -->
<div class="form-group">
    {!! Form::label('fecha_solic_grado', 'Fecha Solic Grado:') !!}
    <p>{!! $person->fecha_solic_grado !!}</p>
</div>

<!-- Fecha Resol Grado Field -->
<div class="form-group">
    {!! Form::label('fecha_resol_grado', 'Fecha Resol Grado:') !!}
    <p>{!! $person->fecha_resol_grado !!}</p>
</div>

<!-- Fecha Resol Prest Field -->
<div class="form-group">
    {!! Form::label('fecha_resol_prest', 'Fecha Resol Prest:') !!}
    <p>{!! $person->fecha_resol_prest !!}</p>
</div>

<!-- Fecha Revision Grado Field -->
<div class="form-group">
    {!! Form::label('fecha_revision_grado', 'Fecha Revision Grado:') !!}
    <p>{!! $person->fecha_revision_grado !!}</p>
</div>

<!-- Cuantia Resol Field -->
<div class="form-group">
    {!! Form::label('cuantia_resol', 'Cuantia Resol:') !!}
    <p>{!! $person->cuantia_resol !!}</p>
</div>

<!-- Tecn Nom Ape Field -->
<div class="form-group">
    {!! Form::label('tecn_nom_Ape', 'Tecn Nom Ape:') !!}
    <p>{!! $person->tecn_nom_Ape !!}</p>
</div>

<!-- Email Tecn Field -->
<div class="form-group">
    {!! Form::label('email_tecn', 'Email Tecn:') !!}
    <p>{!! $person->email_tecn !!}</p>
</div>

<!-- Tel Tecn Field -->
<div class="form-group">
    {!! Form::label('tel_tecn', 'Tel Tecn:') !!}
    <p>{!! $person->tel_tecn !!}</p>
</div>

<!-- Tipo Ayuda Sociosani Field -->
<div class="form-group">
    {!! Form::label('tipo_ayuda_sociosani', 'Tipo Ayuda Sociosani:') !!}
    <p>{!! $person->tipo_ayuda_sociosani !!}</p>
</div>

<!-- Certi Discapacidad Field -->
<div class="form-group">
    {!! Form::label('certi_discapacidad', 'Certi Discapacidad:') !!}
    <p>{!! $person->certi_discapacidad !!}</p>
</div>

<!-- Grado Discapacidad Field -->
<div class="form-group">
    {!! Form::label('grado_discapacidad', 'Grado Discapacidad:') !!}
    <p>{!! $person->grado_discapacidad !!}</p>
</div>

<!-- Fecha Resol Discapacidad Field -->
<div class="form-group">
    {!! Form::label('fecha_resol_discapacidad', 'Fecha Resol Discapacidad:') !!}
    <p>{!! $person->fecha_resol_discapacidad !!}</p>
</div>

<!-- Tipo Discapacidad Field -->
<div class="form-group">
    {!! Form::label('tipo_discapacidad', 'Tipo Discapacidad:') !!}
    <p>{!! $person->tipo_discapacidad !!}</p>
</div>

<!-- Sit Legal Field -->
<div class="form-group">
    {!! Form::label('sit_legal', 'Sit Legal:') !!}
    <p>{!! $person->sit_legal !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $person->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $person->updated_at !!}</p>
</div>

