<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>Calle</th>
        <th>Num Calle</th>
        <th>Puerta Piso</th>
        <th>Cp</th>
        <th>Ciudad</th>
        <th>Isla</th>
        <th>Email</th>
        <th>Whatsapp</th>
        <th>Telefono Domi</th>
        <th>Movil</th>
        <th>Person Id</th>
            <th>Action</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>Calle</th>
        <th>Num Calle</th>
        <th>Puerta Piso</th>
        <th>Cp</th>
        <th>Ciudad</th>
        <th>Isla</th>
        <th>Email</th>
        <th>Whatsapp</th>
        <th>Telefono Domi</th>
        <th>Movil</th>
        <th>Person Id</th>
            <th>Action</th>
        </tr>
    </tfoot>
    <tbody>
    @foreach($contactInfos as $contactInfo)
        <tr>
            <td>{!! $contactInfo->calle !!}</td>
            <td>{!! $contactInfo->num_calle !!}</td>
            <td>{!! $contactInfo->puerta_piso !!}</td>
            <td>{!! $contactInfo->cp !!}</td>
            <td>{!! $contactInfo->ciudad !!}</td>
            <td>{!! $contactInfo->isla !!}</td>
            <td>{!! $contactInfo->email !!}</td>
            <td>{!! $contactInfo->whatsapp !!}</td>
            <td>{!! $contactInfo->telefono_domi !!}</td>
            <td>{!! $contactInfo->movil !!}</td>
            <td>{!! $contactInfo->person_id !!}</td>
            <td>
                {!! Form::open(['route' => ['contactInfos.destroy', $contactInfo->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('contactInfos.show', [$contactInfo->id]) !!}" class='btn btn-primary btn-xs'><i class="far fa-eye"></i></a>
                    <a href="{!! route('contactInfos.edit', [$contactInfo->id]) !!}" class='btn btn-primary btn-xs'><i class="far fa-edit"></i></a>
                    {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>