<!-- Calle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('calle', 'Calle:') !!}
    {!! Form::text('calle', null, ['class' => 'form-control']) !!}
</div>

<!-- Num Calle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('num_calle', 'Num Calle:') !!}
    {!! Form::text('num_calle', null, ['class' => 'form-control']) !!}
</div>

<!-- Puerta Piso Field -->
<div class="form-group col-sm-6">
    {!! Form::label('puerta_piso', 'Puerta Piso:') !!}
    {!! Form::text('puerta_piso', null, ['class' => 'form-control']) !!}
</div>

<!-- Cp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cp', 'Cp:') !!}
    {!! Form::text('cp', null, ['class' => 'form-control']) !!}
</div>

<!-- Ciudad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ciudad', 'Ciudad:') !!}
    {!! Form::text('ciudad', null, ['class' => 'form-control']) !!}
</div>

<!-- Isla Field -->
<div class="form-group col-sm-6">
    {!! Form::label('isla', 'Isla:') !!}
    {!! Form::select('isla', [' Tenerife' => ' Tenerife', ' El Hierro' => ' El Hierro', ' La Palma' => ' La Palma', ' La Gomera' => ' La Gomera', ' Gran Canaria' => ' Gran Canaria', ' Fuerteventura' => ' Fuerteventura', ' Lanzarote' => ' Lanzarote'], null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Telefono Domi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefono_domi', 'Telefono Domi:') !!}
    {!! Form::text('telefono_domi', null, ['class' => 'form-control']) !!}
</div>

<!-- Movil Field -->
<div class="form-group col-sm-6">
    {!! Form::label('movil', 'Movil:') !!}
    {!! Form::text('movil', null, ['class' => 'form-control']) !!}
</div>

<!-- Person Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('person_id', 'Person Id:') !!}
    {!! Form::text('person_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('contactInfos.index') !!}" class="btn btn-default">Cancel</a>
</div>
