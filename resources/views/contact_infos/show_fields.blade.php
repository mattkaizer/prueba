<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $contactInfo->id !!}</p>
</div>

<!-- Calle Field -->
<div class="form-group">
    {!! Form::label('calle', 'Calle:') !!}
    <p>{!! $contactInfo->calle !!}</p>
</div>

<!-- Num Calle Field -->
<div class="form-group">
    {!! Form::label('num_calle', 'Num Calle:') !!}
    <p>{!! $contactInfo->num_calle !!}</p>
</div>

<!-- Puerta Piso Field -->
<div class="form-group">
    {!! Form::label('puerta_piso', 'Puerta Piso:') !!}
    <p>{!! $contactInfo->puerta_piso !!}</p>
</div>

<!-- Cp Field -->
<div class="form-group">
    {!! Form::label('cp', 'Cp:') !!}
    <p>{!! $contactInfo->cp !!}</p>
</div>

<!-- Ciudad Field -->
<div class="form-group">
    {!! Form::label('ciudad', 'Ciudad:') !!}
    <p>{!! $contactInfo->ciudad !!}</p>
</div>

<!-- Isla Field -->
<div class="form-group">
    {!! Form::label('isla', 'Isla:') !!}
    <p>{!! $contactInfo->isla !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $contactInfo->email !!}</p>
</div>

<!-- Whatsapp Field -->
<div class="form-group">
    {!! Form::label('whatsapp', 'Whatsapp:') !!}
    <p>{!! $contactInfo->whatsapp !!}</p>
</div>

<!-- Telefono Domi Field -->
<div class="form-group">
    {!! Form::label('telefono_domi', 'Telefono Domi:') !!}
    <p>{!! $contactInfo->telefono_domi !!}</p>
</div>

<!-- Movil Field -->
<div class="form-group">
    {!! Form::label('movil', 'Movil:') !!}
    <p>{!! $contactInfo->movil !!}</p>
</div>

<!-- Person Id Field -->
<div class="form-group">
    {!! Form::label('person_id', 'Person Id:') !!}
    <p>{!! $contactInfo->person_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $contactInfo->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $contactInfo->updated_at !!}</p>
</div>

