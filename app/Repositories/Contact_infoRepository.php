<?php

namespace App\Repositories;

use App\Models\Contact_info;
use App\Repositories\BaseRepository;

/**
 * Class Contact_infoRepository
 * @package App\Repositories
 * @version April 24, 2019, 5:08 pm UTC
*/

class Contact_infoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'calle',
        'num_calle',
        'puerta_piso',
        'cp',
        'ciudad',
        'isla',
        'email',
        'whatsapp',
        'telefono_domi',
        'movil'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Contact_info::class;
    }
}
