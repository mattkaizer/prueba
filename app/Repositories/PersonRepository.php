<?php

namespace App\Repositories;

use App\Models\Person;
use App\Repositories\BaseRepository;

/**
 * Class PersonRepository
 * @package App\Repositories
 * @version April 24, 2019, 5:08 pm UTC
*/

class PersonRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'n_expediente',
        'fecha_alta_acufade',
        'foto',
        'nombre_completo',
        'alias',
        'genero',
        'fecha_naci',
        'edad',
        'dni',
        'est_civil',
        'grado_dependencia',
        'transporte',
        'transp_dias',
        'trayecto',
        'transp_fecha_init',
        'transp_dir_recogida',
        'cesion_imagen',
        'autoriz_salida_ext',
        'autoriz_salir_centro',
        'socio',
        'num_socio',
        'nivel_educativo',
        'estudios_terminados',
        'ocupacion_anterior',
        'inter_actuales',
        'lee_escribe_ahora',
        'sit_dependencia',
        'grado_dependencia',
        'nivel',
        'tipo_ayuda_depend',
        'fecha_solic_grado',
        'fecha_resol_grado',
        'fecha_resol_prest',
        'fecha_revision_grado',
        'cuantia_resol',
        'tecn_nom_Ape',
        'email_tecn',
        'tel_tecn',
        'tipo_ayuda_sociosani',
        'certi_discapacidad',
        'grado_discapacidad',
        'fecha_resol_discapacidad',
        'tipo_discapacidad',
        'sit_legal'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Person::class;
    }
}
