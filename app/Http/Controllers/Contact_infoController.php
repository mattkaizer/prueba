<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateContact_infoRequest;
use App\Http\Requests\UpdateContact_infoRequest;
use App\Repositories\Contact_infoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class Contact_infoController extends AppBaseController
{
    /** @var  Contact_infoRepository */
    private $contactInfoRepository;

    public function __construct(Contact_infoRepository $contactInfoRepo)
    {
        $this->contactInfoRepository = $contactInfoRepo;
    }

    /**
     * Display a listing of the Contact_info.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $contactInfos = $this->contactInfoRepository->all();

        return view('contact_infos.index')
            ->with('contactInfos', $contactInfos);
    }

    /**
     * Show the form for creating a new Contact_info.
     *
     * @return Response
     */
    public function create()
    {
        return view('contact_infos.create');
    }

    /**
     * Store a newly created Contact_info in storage.
     *
     * @param CreateContact_infoRequest $request
     *
     * @return Response
     */
    public function store(CreateContact_infoRequest $request)
    {
        $input = $request->all();

        $contactInfo = $this->contactInfoRepository->create($input);

        Flash::success('Contact Info saved successfully.');

        return redirect(route('contactInfos.index'));
    }

    /**
     * Display the specified Contact_info.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $contactInfo = $this->contactInfoRepository->find($id);

        if (empty($contactInfo)) {
            Flash::error('Contact Info not found');

            return redirect(route('contactInfos.index'));
        }

        return view('contact_infos.show')->with('contactInfo', $contactInfo);
    }

    /**
     * Show the form for editing the specified Contact_info.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $contactInfo = $this->contactInfoRepository->find($id);

        if (empty($contactInfo)) {
            Flash::error('Contact Info not found');

            return redirect(route('contactInfos.index'));
        }

        return view('contact_infos.edit')->with('contactInfo', $contactInfo);
    }

    /**
     * Update the specified Contact_info in storage.
     *
     * @param int $id
     * @param UpdateContact_infoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateContact_infoRequest $request)
    {
        $contactInfo = $this->contactInfoRepository->find($id);

        if (empty($contactInfo)) {
            Flash::error('Contact Info not found');

            return redirect(route('contactInfos.index'));
        }

        $contactInfo = $this->contactInfoRepository->update($request->all(), $id);

        Flash::success('Contact Info updated successfully.');

        return redirect(route('contactInfos.index'));
    }

    /**
     * Remove the specified Contact_info from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $contactInfo = $this->contactInfoRepository->find($id);

        if (empty($contactInfo)) {
            Flash::error('Contact Info not found');

            return redirect(route('contactInfos.index'));
        }

        $this->contactInfoRepository->delete($id);

        Flash::success('Contact Info deleted successfully.');

        return redirect(route('contactInfos.index'));
    }
}
