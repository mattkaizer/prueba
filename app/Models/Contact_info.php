<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Contact_info
 * @package App\Models
 * @version April 24, 2019, 5:08 pm UTC
 *
 * @property \App\Models\Person person
 * @property string calle
 * @property string num_calle
 * @property string puerta_piso
 * @property string cp
 * @property string ciudad
 * @property string isla
 * @property string email
 * @property string whatsapp
 * @property string telefono_domi
 * @property string movil
 * @property integer person_id
 */
class Contact_info extends Model
{
    use SoftDeletes;

    public $table = 'contact_infos';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'calle',
        'num_calle',
        'puerta_piso',
        'cp',
        'ciudad',
        'isla',
        'email',
        'whatsapp',
        'telefono_domi',
        'movil',
        'person_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'calle' => 'string',
        'num_calle' => 'string',
        'puerta_piso' => 'string',
        'cp' => 'string',
        'ciudad' => 'string',
        'isla' => 'string',
        'email' => 'string',
        'whatsapp' => 'string',
        'telefono_domi' => 'string',
        'movil' => 'string',
        'person_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function person()
    {
        return $this->hasOne(\App\Models\Person::class, 'person_id', 'id');
    }
}
