<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Person
 * @package App\Models
 * @version April 24, 2019, 5:08 pm UTC
 *
 * @property \App\Models\ contact_info contactInfo
 * @property string n_expediente
 * @property string fecha_alta_acufade
 * @property string foto
 * @property string nombre_completo
 * @property string alias
 * @property string genero
 * @property string fecha_naci
 * @property string edad
 * @property string dni
 * @property string est_civil
 * @property string grado_dependencia
 * @property string transporte
 * @property string transp_dias
 * @property string trayecto
 * @property string transp_fecha_init
 * @property string transp_dir_recogida
 * @property string cesion_imagen
 * @property string autoriz_salida_ext
 * @property string autoriz_salir_centro
 * @property string socio
 * @property string num_socio
 * @property string nivel_educativo
 * @property string estudios_terminados
 * @property string ocupacion_anterior
 * @property string inter_actuales
 * @property string lee_escribe_ahora
 * @property string sit_dependencia
 * @property string grado_dependencia
 * @property string nivel
 * @property string tipo_ayuda_depend
 * @property string fecha_solic_grado
 * @property string fecha_resol_grado
 * @property string fecha_resol_prest
 * @property string fecha_revision_grado
 * @property float cuantia_resol
 * @property string tecn_nom_Ape
 * @property string email_tecn
 * @property string tel_tecn
 * @property string tipo_ayuda_sociosani
 * @property string certi_discapacidad
 * @property string grado_discapacidad
 * @property string fecha_resol_discapacidad
 * @property string tipo_discapacidad
 * @property string sit_legal
 */
class Person extends Model
{
    use SoftDeletes;

    public $table = 'Persons';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'n_expediente',
        'fecha_alta_acufade',
        'foto',
        'nombre_completo',
        'alias',
        'genero',
        'fecha_naci',
        'edad',
        'dni',
        'est_civil',
        'grado_dependencia',
        'transporte',
        'transp_dias',
        'trayecto',
        'transp_fecha_init',
        'transp_dir_recogida',
        'cesion_imagen',
        'autoriz_salida_ext',
        'autoriz_salir_centro',
        'socio',
        'num_socio',
        'nivel_educativo',
        'estudios_terminados',
        'ocupacion_anterior',
        'inter_actuales',
        'lee_escribe_ahora',
        'sit_dependencia',
        'grado_dependencia',
        'nivel',
        'tipo_ayuda_depend',
        'fecha_solic_grado',
        'fecha_resol_grado',
        'fecha_resol_prest',
        'fecha_revision_grado',
        'cuantia_resol',
        'tecn_nom_Ape',
        'email_tecn',
        'tel_tecn',
        'tipo_ayuda_sociosani',
        'certi_discapacidad',
        'grado_discapacidad',
        'fecha_resol_discapacidad',
        'tipo_discapacidad',
        'sit_legal'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'n_expediente' => 'string',
        'fecha_alta_acufade' => 'date',
        'foto' => 'string',
        'nombre_completo' => 'string',
        'alias' => 'string',
        'genero' => 'string',
        'fecha_naci' => 'date',
        'edad' => 'string',
        'dni' => 'string',
        'est_civil' => 'string',
        'grado_dependencia' => 'string',
        'transporte' => 'string',
        'transp_dias' => 'string',
        'trayecto' => 'string',
        'transp_fecha_init' => 'date',
        'transp_dir_recogida' => 'string',
        'cesion_imagen' => 'string',
        'autoriz_salida_ext' => 'string',
        'autoriz_salir_centro' => 'string',
        'socio' => 'string',
        'num_socio' => 'string',
        'nivel_educativo' => 'string',
        'estudios_terminados' => 'string',
        'ocupacion_anterior' => 'string',
        'inter_actuales' => 'string',
        'lee_escribe_ahora' => 'string',
        'sit_dependencia' => 'string',
        'grado_dependencia' => 'string',
        'nivel' => 'string',
        'tipo_ayuda_depend' => 'string',
        'fecha_solic_grado' => 'date',
        'fecha_resol_grado' => 'date',
        'fecha_resol_prest' => 'date',
        'fecha_revision_grado' => 'date',
        'cuantia_resol' => 'double',
        'tecn_nom_Ape' => 'string',
        'email_tecn' => 'string',
        'tel_tecn' => 'string',
        'tipo_ayuda_sociosani' => 'string',
        'certi_discapacidad' => 'string',
        'grado_discapacidad' => 'string',
        'fecha_resol_discapacidad' => 'date',
        'tipo_discapacidad' => 'string',
        'sit_legal' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'n_expediente' => 'required, unique',
        'dni' => 'required, unique'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function contactInfo()
    {
        return $this->hasOne(\App\Models\ contact_info::class);
    }
}
