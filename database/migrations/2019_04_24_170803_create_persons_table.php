<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePersonsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Persons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('n_expediente');
            $table->date('fecha_alta_acufade');
            $table->string('foto');
            $table->string('nombre_completo');
            $table->string('alias');
            $table->string('genero');
            $table->date('fecha_naci');
            $table->string('edad');
            $table->string('dni');
            $table->string('est_civil');
            $table->string('grado_dependencia');
            $table->string('transporte');
            $table->string('transp_dias');
            $table->string('trayecto');
            $table->date('transp_fecha_init');
            $table->string('transp_dir_recogida');
            $table->string('cesion_imagen');
            $table->string('autoriz_salida_ext');
            $table->string('autoriz_salir_centro');
            $table->string('socio');
            $table->string('num_socio');
            $table->string('nivel_educativo');
            $table->string('estudios_terminados');
            $table->string('ocupacion_anterior');
            $table->string('inter_actuales');
            $table->string('lee_escribe_ahora');
            $table->string('sit_dependencia');
            $table->string('grado_dependencia');
            $table->string('nivel');
            $table->string('tipo_ayuda_depend');
            $table->date('fecha_solic_grado');
            $table->date('fecha_resol_grado');
            $table->date('fecha_resol_prest');
            $table->date('fecha_revision_grado');
            $table->double('cuantia_resol');
            $table->string('tecn_nom_Ape');
            $table->string('email_tecn');
            $table->string('tel_tecn');
            $table->string('tipo_ayuda_sociosani');
            $table->string('certi_discapacidad');
            $table->string('grado_discapacidad');
            $table->date('fecha_resol_discapacidad');
            $table->string('tipo_discapacidad');
            $table->string('sit_legal');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Persons');
    }
}
