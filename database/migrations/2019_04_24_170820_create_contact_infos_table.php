<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactInfosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('calle');
            $table->string('num_calle');
            $table->string('puerta_piso');
            $table->string('cp');
            $table->string('ciudad');
            $table->string('isla');
            $table->string('email');
            $table->string('whatsapp');
            $table->string('telefono_domi');
            $table->string('movil');
            $table->integer('person_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('person_id')->references('id')->on('person');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contact_infos');
    }
}
